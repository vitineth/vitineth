<img alt="Header picture - green to red gradient formed of randomly sized triangles with the text Ryan Delaney in the lower right corner" src="https://github.com/Vitineth/vitineth/raw/master/name.png" width="100%"/>

Hi, I'm Ryan Delaney and I'm currently a third year computer science student at the University of St Andrews. My account contains a collection of hobby projects and contributions from the past few years.

## 🔭 Current Work

My current work is focussed on my contributions to the University of St Andrews Students Association Ents Crew organisation which I manage, we are currently working to develop an event management and organisation system nicknamed uems. Check it out in the early stages at [ents-crew/uems-hub](https://github.com/ents-crew/uems-hub). As of 12 Jan, this is a near 27'000 line system built on React and Typescript.

## 🌱 Learning

My programming adventures have taken me on quite the journey so far. Right now in my personal projects I'm divided between <img src="https://cdn.jsdelivr.net/gh/Templarian/MaterialDesign/svg/react.svg" height="18"/> React (via [ents-crew/uems-frontend-themis](https://github.com/ents-crew/uems-frontend-themis)), <img src="https://cdn.jsdelivr.net/gh/Templarian/MaterialDesign/svg/react.svg" height="18"/> React Native (via [vitineth/streama-mobile](https://gitlab.com/vitineth/streama-mobile)) and <img src="https://assets.vercel.com/image/upload/v1607554385/repositories/next-js/next-logo.png" height="18"/> Next.js (via [Next Inventory](https://gitlab.com/vitineth/next-inventory)). Through my recent employment (at HubSpot) I've been working with data pipelines and high volume data processing (upwards of 100TB per day) for searching and storage with enterprise systems. And through University (at University of St Andrews) I'll be covering human computer interaction and user interface design, followed by my masters project.

## 📫 Contact

You can check out my website at https://xiomi.org (currently undergoing work so subject to availability) or drop me an email at ryanjdelaney@outlook.com. 

## ⚡ Other

😄 Pronouns: He/Him  
🧱 GitHub: https://github.com/vitineth  
👔 LinkedIn: https://www.linkedin.com/in/ryanjdelaney/

